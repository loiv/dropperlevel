package pl.onyxcherry.loiv.dropperlevel.commands;

import org.bukkit.command.CommandExecutor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import pl.onyxcherry.loiv.dropperlevel.DropperLevel;
import pl.onyxcherry.loiv.dropperlevel.MyDatabase;

public class CommandDropper implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        final int levels_count = 8;
        final String table_name = "dropper";
        final String column_name = "level";

        Player player = (Player) sender;
        String player_name = player.getName();

        if (command.getName().equalsIgnoreCase("dropper")) {
            if (args.length != 1) {
                player.sendMessage("Użycie: " + ChatColor.BLUE + "poziomy, nagroda");
                return true;
            }
            if (!DropperLevel.isPlayerInGroup(player, "dropper." + args[0])) {
                player.sendMessage(ChatColor.RED + "Nie masz uprawnień");
            }

            if (args[0].equalsIgnoreCase("poziomy")) {
                String resp = "";
                boolean[] finished_levels = MyDatabase.getPlayerInfo(player_name, table_name, column_name, levels_count);

                for (int i = 1; i <= levels_count; i++) {
                    if (finished_levels[i] == true) {
                        resp = resp.concat("§2");
                    } else {
                        resp = resp.concat("§c");

                    }
                    resp = resp.concat("Poziom ").concat(String.valueOf(i)).concat("\n");
                }

                if (resp.length() > 0) {
                    player.sendMessage(resp);
                } else {
                    player.sendMessage(ChatColor.RED + "Stało się coś dziwnego. Daj znać administratorom...");
                }
                return true;

            } else if (args[0].equalsIgnoreCase("nagroda"))

            {
                boolean[] finished_levels = MyDatabase.getPlayerInfo(player_name, table_name, column_name, levels_count);
                boolean should_awarded = true;
                boolean received_award = finished_levels[0];

                for (int i = 1; i <= levels_count; i++) {
                    if (finished_levels[i] == false) {
                        should_awarded = false;
                    }
                }

                if (should_awarded && !received_award) {
                    ItemStack diamond = new ItemStack(Material.DIAMOND);
                    diamond.setAmount(1);
                    MyDatabase.markAwardAsReceived(player_name, table_name);
                    player.getInventory().addItem(diamond);
                    player.sendMessage(ChatColor.GOLD + "Brawo!");
                    return true;
                } else if (received_award) {
                    player.sendMessage(ChatColor.RED + "Już odebrałeś nagrodę!");
                    return true;

                } else {
                    player.sendMessage(ChatColor.RED + "Najpierw ukończ wszystkie poziomy!");
                    return true;
                }

            }
        }
        return false;

    }
}