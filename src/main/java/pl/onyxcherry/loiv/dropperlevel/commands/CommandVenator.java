package pl.onyxcherry.loiv.dropperlevel.commands;

import org.bukkit.command.CommandExecutor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import pl.onyxcherry.loiv.dropperlevel.DropperLevel;
import pl.onyxcherry.loiv.dropperlevel.MyDatabase;

public class CommandVenator implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        final int places_count = 6;
        final String table_name = "venator";
        final String column_name = "place";

        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;
        String player_name = player.getName();

        if (command.getName().equalsIgnoreCase("venator")) {
            if (args.length != 1) {
                player.sendMessage("Użycie: " + ChatColor.BLUE + "miejsca, nagroda");
                return true;
            }

            if (!DropperLevel.isPlayerInGroup(player, "venator." + args[0])) {
                player.sendMessage(ChatColor.RED + "Nie masz uprawnień");
            } else if (args[0].equalsIgnoreCase("miejsca")) {
                String resp = "";
                boolean[] reached_places = MyDatabase.getPlayerInfo(player_name, table_name, column_name, places_count);

                for (int i = 1; i <= places_count; i++) {
                    if (reached_places[i] == true) {
                        resp = resp.concat("§2");
                    } else {
                        resp = resp.concat("§c");

                    }
                    resp = resp.concat("Miejsce ").concat(String.valueOf(i)).concat("\n");
                }

                if (resp.length() > 0) {
                    player.sendMessage(resp);
                } else {
                    player.sendMessage(ChatColor.RED + "Stało się coś dziwnego. Daj znać administratorom...");
                }
                return true;

            } else if (args[0].equalsIgnoreCase("nagroda")) {
                boolean[] reached_places = MyDatabase.getPlayerInfo(player_name, table_name, column_name, places_count);
                boolean should_awarded = true;
                boolean received_award = reached_places[0];

                for (int i = 1; i <= places_count; i++) {
                    if (reached_places[i] == false) {
                        should_awarded = false;
                    }
                }
                if (should_awarded && !received_award) {
                    ItemStack diamond = new ItemStack(Material.GOLDEN_HOE);
                    diamond.setAmount(1);
                    MyDatabase.markAwardAsReceived(player_name, table_name);
                    player.getInventory().addItem(diamond);
                    player.sendMessage(ChatColor.GOLD + "Brawo!");
                    return true;
                } else if (received_award) {
                    player.sendMessage(ChatColor.RED + "Już odebrałeś nagrodę!");
                    return true;

                } else {
                    player.sendMessage(ChatColor.RED + "Najpierw ukończ wszystkie poziomy!");
                    return true;
                }
            }
        }
        return false;

    }

}