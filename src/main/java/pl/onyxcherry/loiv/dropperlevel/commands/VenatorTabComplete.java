package pl.onyxcherry.loiv.dropperlevel.commands;

import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class VenatorTabComplete implements TabCompleter {

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {

        if (args.length <= 1) {
            List<String> completions = new ArrayList<String>();
            completions.add("miejsca");
            completions.add("nagroda");
            return completions;
        }
        return null;
    }
}
