package pl.onyxcherry.loiv.dropperlevel;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import net.luckperms.api.LuckPerms;
import pl.onyxcherry.loiv.dropperlevel.commands.CommandDropper;
import pl.onyxcherry.loiv.dropperlevel.commands.CommandVenator;
import pl.onyxcherry.loiv.dropperlevel.commands.DropperTabComplete;
import pl.onyxcherry.loiv.dropperlevel.commands.VenatorTabComplete;
import pl.onyxcherry.loiv.dropperlevel.events.DropperLevelListener;

public class DropperLevel extends JavaPlugin {

    private RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager()
            .getRegistration(LuckPerms.class);

    LuckPerms api = null;

    String dropper_schema = "CREATE TABLE IF NOT EXISTS dropper(id INTEGER PRIMARY KEY AUTOINCREMENT, playername TEXT, level1 BOOL, level2 BOOL, level3 BOOL, level4 BOOL, level5 BOOL, level6 BOOL, level7 BOOL, level8 BOOL, award_received BOOL DEFAULT FALSE)";

    String venator_schema = "CREATE TABLE IF NOT EXISTS venator(id INTEGER PRIMARY KEY AUTOINCREMENT, playername TEXT, place1 BOOL, place2 BOOL, place3 BOOL, place4 BOOL, place5 BOOL, place6 BOOL, award_received BOOL DEFAULT FALSE)";

    @Override
    public void onEnable() {
        getLogger().info("Enabled plugin");
        getServer().getPluginManager().registerEvents(new DropperLevelListener(), this);
        this.getCommand("dropper").setExecutor(new CommandDropper());
        this.getCommand("dropper").setTabCompleter(new DropperTabComplete());
        this.getCommand("venator").setExecutor(new CommandVenator());
        this.getCommand("venator").setTabCompleter(new VenatorTabComplete());

        MyDatabase.connect();
        MyDatabase.createTable(dropper_schema);
        MyDatabase.createTable(venator_schema);

        if (provider != null) {
            api = provider.getProvider();

        }
    }

    @Override
    public void onDisable() {
        getLogger().info("Farewell!");
        MyDatabase.disconnect();
    }

    public LuckPerms getLPProvider() {
        return api;
    }

    public static boolean isPlayerInGroup(Player player, String permission_group) {
        return player.hasPermission(permission_group);
    }

}