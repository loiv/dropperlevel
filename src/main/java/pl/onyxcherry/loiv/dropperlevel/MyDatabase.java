package pl.onyxcherry.loiv.dropperlevel;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MyDatabase {
    public static Connection con;

    public static void connect() {
        File file = new File(DropperLevel.getPlugin(DropperLevel.class).getDataFolder(), "droppersachievements.db");
        if (!file.exists()) {
            new File(DropperLevel.getPlugin(DropperLevel.class).getDataFolder().getPath()).mkdir();
        }

        String URL = "jdbc:sqlite:" + file.getAbsolutePath();
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void disconnect() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createTable(String schema) {
        try {
            PreparedStatement ps = con.prepareStatement(schema);
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean[] getPlayerInfo(String playername, String table_name, String column_name, int count) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean[] finished_levels = new boolean[count + 1];
        // System.out.println("Wywolane");
        try {
            String stmt_to_prepare = String.format("SELECT * FROM %s WHERE playername=?", table_name);
            ps = con.prepareStatement(stmt_to_prepare);
            ps.setString(1, playername);
            rs = ps.executeQuery();

            while (rs.next()) {
                for (int i = 1; i <= count; i++) {
                    finished_levels[i] = rs.getBoolean(column_name + i);
                }
                boolean received_award = rs.getBoolean("award_received");
                finished_levels[0] = received_award;
            }

            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return finished_levels;
    }

    public static void markAwardAsReceived(String playername, String table_name) {
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(
                    String.format("UPDATE %s SET award_received = true WHERE playername = (?)", table_name));
            ps.setString(1, playername);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createPlayerRecord(String player_name, String table_name) {
        PreparedStatement ps = null;
        try {
            String stmt_to_prepare = String.format(
                    "INSERT INTO %s (playername) SELECT ? WHERE NOT EXISTS (SELECT playername FROM %s WHERE playername=?);",
                    table_name, table_name);
            ps = con.prepareStatement(stmt_to_prepare);
            ps.setString(1, player_name);
            ps.setString(2, player_name);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setPlayerInfo(String player_name, String table_name, String record_name) {
        PreparedStatement ps = null;
        try {
            ps = MyDatabase.con.prepareStatement(
                    String.format("UPDATE %s SET %s = true WHERE playername = (?)", table_name, record_name));
            ps.setString(1, player_name);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
