package pl.onyxcherry.loiv.dropperlevel.events;

import pl.onyxcherry.loiv.dropperlevel.MyDatabase;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;

import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class DropperLevelListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player current_player = event.getPlayer();
        String current_player_name = current_player.getName();

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            BlockState clicked_block_state = event.getClickedBlock().getState();

            if (clicked_block_state instanceof Sign) {
                Sign clicked_sign = (Sign) clicked_block_state;
                String zero_line = clicked_sign.getLine(0);

                if (zero_line.contains("Warp")) {
                    Location loc = BukkitAdapter.adapt(current_player.getLocation());
                    RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
                    RegionQuery query = container.createQuery();
                    ApplicableRegionSet set = query.getApplicableRegions(loc);

                    for (ProtectedRegion region : set) {
                        String region_name = region.getId();

                        if (region_name.startsWith("dropper")) {
                            String region_dropper_level = region_name.substring(7, region_name.length());
                            String message_to_log = String.format("Gracz %s przeszedł poziom %s", current_player_name,
                                    region_dropper_level);
                            Bukkit.getLogger().info(message_to_log);
                            MyDatabase.setPlayerInfo(current_player_name, "dropper",
                                    "level" + String.valueOf(region_dropper_level));

                        }
                    }

                } else if (zero_line.contains("Venator")) {
                    String venator_place_number = clicked_sign.getLine(1);
                    String message_to_log = String.format("Gracz %s doszedł do miejsca %s", current_player_name,
                            venator_place_number);
                    Bukkit.getLogger().info(message_to_log);
                    MyDatabase.setPlayerInfo(current_player_name, "venator", "place" + venator_place_number);

                }
            }
        }
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        Player current_player = event.getPlayer();
        String current_player_name = current_player.getName();
        MyDatabase.createPlayerRecord(current_player_name, "dropper");
        MyDatabase.createPlayerRecord(current_player_name, "venator");
    }
}
